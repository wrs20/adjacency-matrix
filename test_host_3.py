#!/usr/bin/python

import ctypes
import numpy as np
import math
import os


import ppmd as md
import adj_loops


N = 1000
crN = 10 #cubert(N)
E = 8.

Eo2 = E/2.

tol = 10.**(-14)

rank = md.mpi.MPI_HANDLE.rank
nproc = md.mpi.MPI_HANDLE.nproc


PositionDat = md.data.PositionDat
ParticleDat = md.data.ParticleDat
ScalarArray = md.data.ScalarArray
State = md.state.State
ParticleLoop = md.loop.ParticleLoop
Pairloop = md.pairloop.PairLoopNeighbourListNS

# ----------------------------------------------------------------------


# create a state called A
A = State()


# load vlaues from a DL_POLY CONFIG
DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    './res/1k_1k_08442_25/'
)

CONFIG = DIR + '/CONFIG'
CONTROL = DIR + '/CONTROL'
FIELD = DIR + '/FIELD'

rFIELD = md.utility.dl_poly.read_field(FIELD)
rCONTROL = md.utility.dl_poly.read_control(CONTROL)

A.npart = int(md.utility.dl_poly.get_field_value(rFIELD, 'NUMMOLS')[0][0])
potaa_rc = float(md.utility.dl_poly.get_control_value(rCONTROL, 'cutoff')[0][0])
potaa_rn = potaa_rc * 1.1


# Init the domain in A
extent = md.utility.dl_poly.read_domain_extent(CONFIG)
A.domain = md.domain.BaseDomainHalo(extent=extent)
A.domain.boundary_condition = md.domain.BoundaryTypePeriodic()


# init particle dats for md part
A.p = PositionDat(ncomp=3)
A.v = ParticleDat(ncomp=3)
A.f = ParticleDat(ncomp=3)
A.mass = ParticleDat(ncomp=1)
A.gid = ParticleDat(ncomp=1, dtype=ctypes.c_int)

A.u = ScalarArray(ncomp=2, name='u')
A.ke = ScalarArray(ncomp=2, name='ke')

# adj matrix specific dats/vars
NMAX = 20
A.nenv = ParticleDat(ncomp=1, dtype=ctypes.c_int)
A.env = ParticleDat(ncomp=NMAX, dtype=ctypes.c_int)
A.A = ParticleDat(ncomp=NMAX*NMAX, dtype=ctypes.c_int)


# load DL_POLY CONFIG values into dats
A.gid[:, 0] = np.arange(A.npart)
A.p[:] = md.utility.dl_poly.read_positions(CONFIG)
A.v[:] = md.utility.dl_poly.read_velocities(CONFIG)
A.f[:] = md.utility.dl_poly.read_forces(CONFIG)

A.u[0] = 0.0
A.ke[0] = 0.0

A.mass[:, 0] = np.ones(A.npart) * \
            float(md.utility.dl_poly.get_field_value(
                rFIELD,
                'Ar'
            )[0][0])

A.npart_local = A.npart
A.filter_on_domain_boundary()


# create a buckingham potential between particles
potaa = md.potential.Buckingham(
    a=1.69*10**-8.0,
    b=1.0/0.273,
    c=102*10**-12,
    rc=potaa_rc
)

# pairloop to update forces and potential energy
potaa_force_updater = Pairloop(
    kernel=potaa.kernel,
    dat_dict=potaa.get_data_map(
        positions=A.p,
        forces=A.f,
        potential_energy=A.u
    ),
    shell_cutoff=potaa_rn
)


# adjacency matrix loops from adj_loops module.
# this class provides an "execute" method we can pass to the integrator
# scheduler
# connected_r = 1.0
# adj_handle = adj_loops.AdjHandle(NMAX, potaa_rn, connected_r, A.p, A.gid,
#                                  A.nenv, A.env, A.A)



dt = 0.0001

vv_kernel1_code = '''
const double M_tmp = 1.0/M(0);
V(0) += dht*F(0)*M_tmp;
V(1) += dht*F(1)*M_tmp;
V(2) += dht*F(2)*M_tmp;
P(0) += dt*V(0);
P(1) += dt*V(1);
P(2) += dt*V(2);
'''

vv_kernel2_code = '''
const double M_tmp = 1.0/M(0);
V(0) += dht*F(0)*M_tmp;
V(1) += dht*F(1)*M_tmp;
V(2) += dht*F(2)*M_tmp;
k(0) += (V(0)*V(0) + V(1)*V(1) + V(2)*V(2))*0.5*M(0);
'''
constants = [
    md.kernel.Constant('dt', dt),
    md.kernel.Constant('dht', 0.5*dt),
]

vv_kernel1 = md.kernel.Kernel('vv1', vv_kernel1_code, constants)
vv_p1 = ParticleLoop(
    n=A.as_func('npart_local'),
    kernel=vv_kernel1,
    particle_dat_dict={'P': A.p(md.access.W),
                       'V': A.v(md.access.W),
                       'F': A.f(md.access.R),
                       'M': A.mass(md.access.R)}
)

vv_kernel2 = md.kernel.Kernel('vv2', vv_kernel2_code, constants)
vv_p2 = ParticleLoop(
    n=A.as_func('npart_local'),
    kernel=vv_kernel2,
    particle_dat_dict={'V': A.v(md.access.W),
                       'F': A.f(md.access.R),
                       'M': A.mass(md.access.R),
                       'k': A.ke(md.access.INC0)}
)

ke_list = []
u_list = []


for it in md.method.IntegratorRange(1000, dt, A.v, 10, 0.25):

    vv_p1.execute(A.npart_local)
    potaa_force_updater.execute()
    vv_p2.execute(A.npart_local)

    if it % 10 == 0:
        ke_list.append(A.ke[0])
        u_list.append(A.u[0])





# reduce the energy arrays
ke_array = md.mpi.all_reduce(np.array(ke_list))
u_array = md.mpi.all_reduce(np.array(u_list))

print ke_array[0] + u_array[0]
print ke_array[-1] + u_array[-1]











