#!/usr/bin/python

"""
Adjacency matrix code written to only use asymmetric
kernels.
"""

import numpy as np
import ctypes

from ppmd import *

PositionDat = data.PositionDat
ParticleDat = data.ParticleDat
ScalarArray = data.ScalarArray
State = state.State

NMAX = 100
r_co = 2.0
r_ci = 1.1

# Extent E
E = 4.0

pbool = False

N = NMAX
p = np.random.uniform(-0.5*E, 0.5*E, [N,3])


# utility.dl_poly.read_domain_extent('data/TEST7/CONFIG')
# utility.dl_poly.read_positions('data/TEST7/CONFIG')

# #############################################################


A = State()
A.npart = N
A.domain = domain.BaseDomainHalo(extent=(E, E, E))
A.p = PositionDat(ncomp=3)
A.gid = ParticleDat(ncomp=1, dtype=ctypes.c_int)
A.nenv = ParticleDat(ncomp=1, dtype=ctypes.c_int)
A.env = ParticleDat(ncomp=NMAX, dtype=ctypes.c_int)
A.A = ParticleDat(ncomp=NMAX*NMAX, dtype=ctypes.c_int)





A.p[:] = p
A.gid[:, 0] = np.arange(N)
A.scatter_data_from(0)
# A.filter_on_domain_boundary()


# print mpi.MPI_HANDLE.rank, A.npart_local, A.npart, A.p[:]

# #############################################################

NMAXc = kernel.Constant('N_max', NMAX)
r_coc = kernel.Constant('Lambda', r_co)
r_cic = kernel.Constant('R', r_ci)





k_init_code = '''

nenv(0) = 1;
env(0) = globalid(0);
for(int px=0 ; px<N_max ; px++){
    A((N_max+1)*px) = 1;
}

'''

k_init = kernel.Kernel(name='init', code=k_init_code, constants=(NMAXc,))
# Run as particle loop

k_init_map = {'globalid': A.gid(access.R),
              'nenv': A.nenv(access.W),
              'env': A.env(access.W),
              'A': A.A(access.W)}

loop_init = loop.ParticleLoop(kernel=k_init, particle_dat_dict=k_init_map)







k_connect_direct_code = '''

double dr2=0;
for (int i=0 ; i<3 ; ++i) {
  double dx= position(0,i) - position(1,i);
  dr2 += dx*dx;
}


// Only execute for local environment
if (dr2 < Lambda*Lambda) {


  // Add global id of other particle to global env list
  env(0,nenv(0,0)) = globalid(1,0);

  // If particles are connected, set entries A^{(i)}_{ij} and
  // A^{(i)}_{ji} in local adjacency matrices
  if (dr2 < R*R) {


    A(0, nenv(0, 0)) = 1;               // A^{(i)}_{ij} = 1
    A(0, N_max*nenv(0, 0)) = 1;         // A^{(i)}_{ji} = 1
  }

  nenv(0,0)++;

}
'''

k_connect_direct = kernel.Kernel(name='connect_direct',
                                 code=k_connect_direct_code,
                                 constants=(NMAXc, r_coc, r_cic))

k_connect_direct_map = {'position': A.p(access.R),
                        'globalid': A.gid(access.R),
                        'nenv': A.nenv(access.W),
                        'env': A.env(access.W),
                        'A': A.A(access.W)}

loop_connect_direct = pairloop.PairLoopNeighbourListNS(kernel=k_connect_direct,
                                                     dat_dict=k_connect_direct_map,
                                                     shell_cutoff=r_co)


k_connect_indirect_code = '''

// Calculate distance dr2 = ||p_0 - p_2|| between particles
double dr2=0;
for (int i=0 ; i<3 ; ++i) {
    double dx = position(0, i) - position(1, i);
    dr2 += dx*dx;
}

if (dr2 < Lambda*Lambda) {

    for (int i=0; i<nenv(0, 0) ; ++i) {

        // Find the local index i in the environment of this
        // particle which corresponds to the other particle
        if (env(0, i) == globalid(1, 0)) {

            // Loop over all local indices k_this of particles in this
            // particle's environment local indices k_other of particles in the
            // other particle's environment

            for (int k_this=0; k_this<nenv(0, 0) ; ++k_this) {
                for (int k_other=0; k_other<nenv(1, 0); ++k_other) {

                    // Check of k_this and k_other correspond to the same
                    // physical particle AND the particle is connected to the
                    // other particle (i.e. check if A^{(j)}_{jk} == 1)

                    if ( (env(0, k_this) == env(1, k_other)) &&
                         (A(1, k_other) == 1) ) {

                         // If this is the case, set A^{(i)}_{jk} A^{(i)}_{kj} = 1
                         A(0, N_max*i + k_this ) = 1;
                         A(0, N_max*k_this + i ) = 1;
                    }
                }
            }
        }
    }
}


'''

k_connect_indirect = kernel.Kernel(name='connect_indirect',
                                   code=k_connect_indirect_code,
                                   constants=(NMAXc, r_coc, r_cic))

k_connect_indirect_map = {'position': A.p(access.R),
                          'globalid': A.gid(access.R),
                          'nenv': A.nenv(access.R),
                          'env': A.env(access.R),
                          'A': A.A(access.RW)}

loop_connect_indirect = pairloop.PairLoopNeighbourListNS(kernel=k_connect_indirect,
                                                       dat_dict=k_connect_indirect_map,
                                                       shell_cutoff=r_co)


np.set_printoptions(linewidth=2*N+2)
loop_init.execute()

if pbool:
    if mpi.MPI_HANDLE.rank == 0:
        print A.A[:N:, ::]
        print A.env[:N:, ::]

    print 60*"-"
loop_connect_direct.execute()

if pbool:
    if mpi.MPI_HANDLE.rank == 0:
        print A.A[:N:, ::]
        print A.env[:N:, ::]

    print 60*"-"
loop_connect_indirect.execute()

if pbool:
    A.gather_data_on()

    if mpi.MPI_HANDLE.rank == 0:
        print A.A[:N:, ::]
        print A.env[:N:, ::]







