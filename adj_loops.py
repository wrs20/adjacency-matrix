#!/usr/bin/python

"""
Adjacency matrix code written to only use asymmetric
kernels.
"""

from ppmd import *


def loop1(
        nmax,
        gid_dat,
        nenv_dat,
        env_dat,
        a_dat
    ):
    NMAXc = kernel.Constant('N_max', nmax)

    k_init_code = '''

    nenv(0) = 1;
    env(0) = globalid(0);
    for(int px=0 ; px<N_max ; px++){
        A((N_max+1)*px) = 1;
    }

    '''

    k_init = kernel.Kernel(name='init', code=k_init_code, constants=(NMAXc,))
    # Run as particle loop

    k_init_map = {'globalid': gid_dat(access.R),
                  'nenv': nenv_dat(access.W),
                  'env': env_dat(access.W),
                  'A': a_dat(access.W)}

    return loop.ParticleLoop(kernel=k_init, particle_dat_dict=k_init_map)


def loop2(
        nmax,
        r_co,
        r_ci,
        p_dat,
        gid_dat,
        nenv_dat,
        env_dat,
        a_dat
    ):
    NMAXc = kernel.Constant('N_max', nmax)
    r_coc = kernel.Constant('Lambda', r_co)
    r_cic = kernel.Constant('R', r_ci)

    k_connect_direct_code = '''

    double dr2=0;
    for (int i=0 ; i<3 ; ++i) {
      double dx= position(0,i) - position(1,i);
      dr2 += dx*dx;
    }


    // Only execute for local environment
    if (dr2 < Lambda*Lambda) {


      // Add global id of other particle to global env list
      env(0,nenv(0,0)) = globalid(1,0);

      // If particles are connected, set entries A^{(i)}_{ij} and
      // A^{(i)}_{ji} in local adjacency matrices
      if ( (dr2 < R*R) && (nenv(0,0) < N_max) ) {

        A(0, nenv(0, 0)) = 1;               // A^{(i)}_{ij} = 1
        A(0, N_max*nenv(0, 0)) = 1;         // A^{(i)}_{ji} = 1

      }

      nenv(0,0)++;

    }
    '''

    k_connect_direct = kernel.Kernel(name='connect_direct',
                                     code=k_connect_direct_code,
                                     constants=(NMAXc, r_coc, r_cic))

    k_connect_direct_map = {'position': p_dat(access.R),
                            'globalid': gid_dat(access.R),
                            'nenv': nenv_dat(access.W),
                            'env': env_dat(access.W),
                            'A': a_dat(access.W)}

    return pairloop.PairLoopNeighbourListNS(kernel=k_connect_direct,
                                            dat_dict=k_connect_direct_map,
                                            shell_cutoff=r_co)


def loop3(
        nmax,
        r_co,
        r_ci,
        p_dat,
        gid_dat,
        nenv_dat,
        env_dat,
        a_dat
    ):

    NMAXc = kernel.Constant('N_max', nmax)
    r_coc = kernel.Constant('Lambda', r_co)
    r_cic = kernel.Constant('R', r_ci)

    k_connect_indirect_code = '''

    // Calculate distance dr2 = ||p_0 - p_2|| between particles
    double dr2=0;
    for (int i=0 ; i<3 ; ++i) {
        double dx = position(0, i) - position(1, i);
        dr2 += dx*dx;
    }

    if (dr2 < Lambda*Lambda) {

        for (int i=0; i<nenv(0, 0) ; ++i) {

            // Find the local index i in the environment of this
            // particle which corresponds to the other particle
            if (env(0, i) == globalid(1, 0)) {

                // Loop over all local indices k_this of particles in this
                // particle's environment local indices k_other of particles in the
                // other particle's environment

                for (int k_this=0; k_this<nenv(0, 0) ; ++k_this) {
                    for (int k_other=0; k_other<nenv(1, 0); ++k_other) {

                        // Check of k_this and k_other correspond to the same
                        // physical particle AND the particle is connected to the
                        // other particle (i.e. check if A^{(j)}_{jk} == 1)

                        if ( (env(0, k_this) == env(1, k_other)) &&
                             (A(1, k_other) == 1) ) {

                             // If this is the case, set A^{(i)}_{jk} A^{(i)}_{kj} = 1
                             A(0, N_max*i + k_this ) = 1;
                             A(0, N_max*k_this + i ) = 1;
                        }
                    }
                }
            }
        }
    }


    '''

    k_connect_indirect = kernel.Kernel(name='connect_indirect',
                                       code=k_connect_indirect_code,
                                       constants=(NMAXc, r_coc, r_cic))

    k_connect_indirect_map = {'position': p_dat(access.R),
                              'globalid': gid_dat(access.R),
                              'nenv': nenv_dat(access.R),
                              'env': env_dat(access.R),
                              'A': a_dat(access.RW)}

    return pairloop.PairLoopNeighbourListNS(kernel=k_connect_indirect,
                                            dat_dict=k_connect_indirect_map,
                                            shell_cutoff=r_co)


class AdjHandle(object):
    def __init__(
        self,
        nmax,
        r_co,
        r_ci,
        p_dat,
        gid_dat,
        nenv_dat,
        env_dat,
        a_dat
    ):
        self.env = env_dat
        self.adj_loop1 = loop1(
            nmax,
            gid_dat,
            nenv_dat,
            env_dat,
            a_dat
        )

        self.adj_loop2 = loop2(
            nmax,
            r_co,
            r_ci,
            p_dat,
            gid_dat,
            nenv_dat,
            env_dat,
            a_dat
        )
        self.adj_loop3 = loop3(
            nmax,
            r_co,
            r_ci,
            p_dat,
            gid_dat,
            nenv_dat,
            env_dat,
            a_dat
        )

    def execute(self):
        self.adj_loop1.execute()
        self.adj_loop2.execute()
        self.adj_loop3.execute()
        #self.env.gather_data_on(0)
        if mpi.MPI_HANDLE.rank == 0:
            print 80*'='
            print self.env[:20:, :]





