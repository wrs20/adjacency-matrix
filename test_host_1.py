#!/usr/bin/python

import ctypes
import numpy as np
import math
import os


import ppmd as md
import adj_loops


N = 1000
crN = 10 #cubert(N)
E = 8.

Eo2 = E/2.

tol = 10.**(-14)

rank = md.mpi.MPI_HANDLE.rank
nproc = md.mpi.MPI_HANDLE.nproc


PositionDat = md.data.PositionDat
ParticleDat = md.data.ParticleDat
ScalarArray = md.data.ScalarArray
State = md.state.State

# ----------------------------------------------------------------------


# create a state called A
A = State()


# load vlaues from a DL_POLY CONFIG
DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    './res/1k_1k_08442_25/'
)

CONFIG = DIR + '/CONFIG'
CONTROL = DIR + '/CONTROL'
FIELD = DIR + '/FIELD'

rFIELD = md.utility.dl_poly.read_field(FIELD)
rCONTROL = md.utility.dl_poly.read_control(CONTROL)

A.npart = int(md.utility.dl_poly.get_field_value(rFIELD, 'NUMMOLS')[0][0])
potaa_rc = float(md.utility.dl_poly.get_control_value(rCONTROL, 'cutoff')[0][0])
potaa_rn = potaa_rc * 1.1


# Init the domain in A
extent = md.utility.dl_poly.read_domain_extent(CONFIG)
A.domain = md.domain.BaseDomainHalo(extent=extent)
A.domain.boundary_condition = md.domain.BoundaryTypePeriodic()


# init particle dats for md part
A.p = PositionDat(ncomp=3)
A.v = ParticleDat(ncomp=3)
A.f = ParticleDat(ncomp=3)
A.mass = ParticleDat(ncomp=1)
A.gid = ParticleDat(ncomp=1, dtype=ctypes.c_int)

A.u = ScalarArray(ncomp=2)

# adj matrix specific dats/vars
NMAX = 20
A.nenv = ParticleDat(ncomp=1, dtype=ctypes.c_int)
A.env = ParticleDat(ncomp=NMAX, dtype=ctypes.c_int)
A.A = ParticleDat(ncomp=NMAX*NMAX, dtype=ctypes.c_int)


# load DL_POLY CONFIG values into dats
A.gid[:,0] = np.arange(A.npart)
A.p[:] = md.utility.dl_poly.read_positions(CONFIG)
A.v[:] = md.utility.dl_poly.read_velocities(CONFIG)
A.f[:] = md.utility.dl_poly.read_forces(CONFIG)
A.u[0] = 0.0

A.mass[:, 0] = np.ones(A.npart) * \
            float(md.utility.dl_poly.get_field_value(
                rFIELD,
                'Ar'
            )[0][0])

A.npart_local = A.npart
A.filter_on_domain_boundary()


# create a buckingham potential between particles
potaa = md.potential.Buckingham(
    a=1.69*10**-8.0,
    b=1.0/0.273,
    c=102*10**-12,
    rc=potaa_rc
)

# pairloop to update forces and potential energy
potaa_force_updater = md.pairloop.PairLoopNeighbourListNS(
    kernel = potaa.kernel,
    dat_dict= potaa.get_data_map(
        positions=A.p,
        forces=A.f,
        potential_energy=A.u
    ),
    shell_cutoff=potaa_rn
)

# tool to keep a log of kinetic energy over time
potaa_kinetic_energy_updater = md.method.KineticEnergyTracker(
    velocities=A.v,
    masses=A.mass
)

# tool to keep a log of potential energy over time
potaa_potential_energy = md.method.PotentialEnergyTracker(
    potential_energy_dat=A.u
)


# adjacency matrix loops from adj_loops module.
# this class provides an "execute" method we can pass to the integrator
# scheduler
connected_r = 1.0
adj_handle = adj_loops.AdjHandle(NMAX, potaa_rn, connected_r, A.p, A.gid,
                                 A.nenv, A.env, A.A)

# create a schedule that logs potential/kinetic energy every step
# and executes adjacency matrix loops every 100 steps
potaa_schedule = md.method.Schedule(
    [1, 1, 100],
    [
        potaa_kinetic_energy_updater.execute,
        potaa_potential_energy.execute,
        adj_handle.execute
    ]
)

# create an instance of an integrator
potaa_integrator = md.method.IntegratorVelocityVerlet(
    positions=A.p,
    forces=A.f,
    velocities=A.v,
    masses=A.mass,
    force_updater=potaa_force_updater,
    interaction_cutoff=potaa_rc,
    list_reuse_count=10
)

# integrate forward in time
potaa_integrator.integrate(0.001, 10., potaa_schedule)


# debug printing
print potaa_kinetic_energy_updater.get_kinetic_energy_array()[0] , \
    potaa_potential_energy.get_potential_energy_array()[0], \
    potaa_kinetic_energy_updater.get_kinetic_energy_array()[-1] , \
    potaa_potential_energy.get_potential_energy_array()[-1]







